/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto2_oo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author andre
 */
public class JogadorTest {
    
    Jogador jogador;
    
    public JogadorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        jogador = new Jogador(6,6);
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getIsPlaying method, of class Jogador.
     */
    @Test
    public void testGetIsPlaying() {
        boolean expResult = true;
        boolean result = jogador.getIsPlaying();
        assertEquals(expResult, result);

    }

    /**
     * Test of setIsPlaying method, of class Jogador.
     */
    @Test
    public void testSetIsPlaying() {
        boolean isPlaying = false;
        jogador.setIsPlaying(isPlaying);
        assertEquals(jogador.getIsPlaying(), isPlaying);
    }

    /**
     * Test of getMapa method, of class Jogador.
     */
    @Test
    public void testGetMapa() {
        int[][][] expResult = new int[6][6][2];
        for(int aux = 0;aux<6;aux++){
            for(int aux2=0;aux2<6;aux2++){
                expResult[aux][aux2][0] = 0;
                expResult[aux][aux2][1] = 0;
            }
        }
        int[][][] result = jogador.getMapa();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of setMapa method, of class Jogador.
     */
    @Test
    public void testSetMapa() {
        int[][][] mapa = new int[6][6][2];
        for(int aux = 0;aux<6;aux++){
            for(int aux2=0;aux2<6;aux2++){
                mapa[aux][aux2][0] = 1;
                mapa[aux][aux2][1] = 1;
            }
        }
        jogador.setMapa(mapa);
        assertArrayEquals(jogador.getMapa(), mapa);
    }

    /**
     * Test of getRecursos method, of class Jogador.
     */
    @Test
    public void testGetRecursos() {
        int expResult = 0;
        int result = jogador.getRecursos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setRecursos method, of class Jogador.
     */
    @Test
    public void testSetRecursos() {
        int recursos = 0;
        jogador.setRecursos(recursos);
        assertEquals(jogador.getRecursos(), recursos);
    }
    
}
