2º Exercício Prático - Orientação a Objetos 2018.1 - UnB - Gama
=========================
André Lucas de Sousa Pinto - 17/0068251

## Instalação

1. Faça o clone deste projeto com git clone https://gitlab.com/andrelucax/Projeto2-OO.git
2. Abra o IDE NetBeans e importe o projeto
3. Rode o programa via o comando run da IDE NetBeans
4. Execute as ações de acordo com o que é oferecido no menu

## Funcionamento

**Janela Inicial (Menu de Opções)**
```
	[JOGAR]
	Clique no botão JOGAR e selecione o arquivo do mapa 
	
	[RANK]
	Clique no botão RANK e olhe o TOP 10 melhores scores
```
	
**Segunda Janela (Janela do Jogo)**
```
	[ATACAR UNICA CELULA]
	Com esta opção marcada o jogador pode escolher uma posição do mapa para atirar
	Opção unica celula pega o local do cursor e atira na celula clicada
	Ao acertar agua é aberto um gif de água no local do tiro
	Ao acertar um navio é aberto um quadrado vermelho no local do tiro
	Ao destruir um navio é aberto o quadrado referente ao navio na cor verde
	
	[DESCOBRIR AREA 2X2]
	Com esta opção marcada o jogador pode escolher uma area 2x2 para descobrir
	Opção 2x2 pega o local do cursor e descobre a celula clicada, a adjacente direita e as 2 a baixo das citadas
	Ao descobrir agua é aberto um quadrado cinza no local do tiro
	Ao descobrir um navio é aberto um quadrado rosa no local do tiro
	
	[ATACAR AREA 2X2]
	Com esta opção marcada o jogador pode escolher uma area 2x2 para atacar
	Opção 2x2 pega o local do cursor e ataca a celula clicada, a adjacente direita e as 2 a baixo das citadas
	Ao acertar agua são abertos gifs de água nos locais atirados
	Ao acertar um navio são abertos quadrados vermelhos nos locais atirados
	Ao destruir um navio são abertos quadrados referentes ao navio na cor verde
	
	[ATACAR LINHA]
	Com esta opção marcada o jogador pode escolher uma linha do mapa para atirar
	Opção linha pega o local do cursor e ataca a linha da celula clicada
	Ao acertar agua são abertos gifs de água nos locais atirados
	Ao acertar um navio são abertos quadrados vermelhos nos locais atirados
	Ao destruir um navio são abertos quadrados referentes ao navio na cor verde
	
	[ATACAR COLUNA]
	Com esta opção marcada o jogador pode escolher um quadrado do mapa para atirar
	Opção linha pega o local do cursor e ataca a coluna da celula clicada
	Ao acertar agua são abertos gifs de água nos locais atirados
	Ao acertar um navio são abertos quadrados vermelhos nos locais atirados
	Ao destruir um navio são abertos quadrados referentes ao navio na cor verde
	
	[INFO]
	Mostra um breve tutorial de como funiona o jogo
```

**Condição de vitória/derrota e recursos**
```
	[CONDIÇÃO DE VITÓRIA]
	O jogador vence caso destrua todas as embarcações antes de acabar com o recurso disponivel
	
	[CONDIÇÃO DE DERROTA]
	O jogador perde caso acabe com o recurso disponivel antes de destruir todas as embarcações
	
	[RECURSOS]
	Os recursos são dados de acordo com o número de navios em jogo sendo possível atacar no máximo todos os navios e metade da agua

```
