package projeto2_oo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author andre
 */
public class JanelaInicial extends javax.swing.JFrame {

    private boolean isPlaying = false;
    private int largura = 0;
    private int altura = 0;
    int[][][] mapa;

    /**
     * Creates new form JanelaInicial
     */
    public JanelaInicial() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label1 = new java.awt.Label();
        label2 = new java.awt.Label();
        Jogar = new java.awt.Button();
        button1 = new java.awt.Button();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        label1.setAlignment(java.awt.Label.CENTER);
        label1.setFont(new java.awt.Font("Purisa", 1, 48)); // NOI18N
        label1.setText("2.0");

        label2.setAlignment(java.awt.Label.CENTER);
        label2.setFont(new java.awt.Font("Purisa", 1, 48)); // NOI18N
        label2.setText("Batalha Naval");

        Jogar.setFont(new java.awt.Font("Purisa", 1, 36)); // NOI18N
        Jogar.setLabel("Jogar");
        Jogar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JogarActionPerformed(evt);
            }
        });

        button1.setFont(new java.awt.Font("Purisa", 1, 36)); // NOI18N
        button1.setLabel("Rank");
        button1.setMinimumSize(new java.awt.Dimension(138, 72));
        button1.setPreferredSize(new java.awt.Dimension(138, 72));
        button1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(228, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(139, 139, 139)
                        .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(button1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Jogar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(107, 107, 107)))
                .addGap(220, 220, 220))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(85, Short.MAX_VALUE)
                .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(Jogar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(button1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(69, 69, 69))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void JogarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JogarActionPerformed
        JOptionPane.showMessageDialog(null, "Selecione o mapa do jogo");
        JFileChooser file = new JFileChooser(); 
        file.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int i = file.showSaveDialog(null);
        if (i==1){
            // cancelou aberta do arquivo
        } 
        else {
            // abriu arquivo
            File arquivo = file.getSelectedFile();
            if (arquivo.canRead()){
                try{
                    BufferedReader br = new BufferedReader(new FileReader(arquivo.getAbsolutePath()));
                    String auxS = br.readLine();
                    auxS = br.readLine();
                    String stringDaLargura = "";
                    String stringDaAltura = "";
                    for(int aux = 0; true; aux++){
                        if (auxS.toCharArray()[aux] == ' '){
                            for (int aux2 = aux + 1 ; true; aux2++){
                                try{
                                    stringDaAltura = stringDaAltura + auxS.toCharArray()[aux2];
                                }
                                catch (Exception e){
                                    break;
                                }
                            }
                            break;
                        }
                        stringDaLargura = stringDaLargura + auxS.toCharArray()[aux];
                    }
                    largura = Integer.parseInt(stringDaLargura);
                    altura = Integer.parseInt(stringDaAltura);
                    
                    auxS = br.readLine();
                    auxS = br.readLine();
                    mapa = new int[largura][altura][2];
                    for (int aux = 0; aux < altura; aux++){
                        auxS = br.readLine();
                        for (int aux2 = 0; aux2 < largura; aux2++){
                            String straux = "" + auxS.toCharArray()[aux2];
                            mapa[aux2][aux][Jogador.EMBARCACAOMAPA] = Integer.parseInt(straux);
                            mapa[aux2][aux][Jogador.FIREDMAPA] = Jogador.NOTFIRED;
                        }
                    }
                    if (largura < 6 || altura < 6){
                        Exception exp = new Exception();
                        throw (exp);
                    }
                    br.close();
                    isPlaying = true;

		}
                catch(Exception e){
                    JOptionPane.showMessageDialog(null, "Não foi possível abrir o arquivo ou o mesmo não possui informações do mapa");
		}
            }
            else{
                JOptionPane.showMessageDialog(null, "Não foi possível abrir o arquivo");
            }
        }
    }//GEN-LAST:event_JogarActionPerformed

    private void button1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button1ActionPerformed
        // TODO add your handling code here:
        Records record = new Records();
        JOptionPane.showMessageDialog(null, "1: " + String.valueOf(record.getRecord()[0]) + "\n"
                                            + "2: " + String.valueOf(record.getRecord()[1]) + "\n"
                                            + "3: " + String.valueOf(record.getRecord()[2]) + "\n"
                                            + "4: " + String.valueOf(record.getRecord()[3]) + "\n"
                                            + "5: " + String.valueOf(record.getRecord()[4]) + "\n"
                                            + "6: " + String.valueOf(record.getRecord()[5]) + "\n"
                                            + "7: " + String.valueOf(record.getRecord()[6]) + "\n"
                                            + "8: " + String.valueOf(record.getRecord()[7]) + "\n"
                                            + "9: " + String.valueOf(record.getRecord()[8]) + "\n"
                                            + "10 : " + String.valueOf(record.getRecord()[9]) + "\n");
    }//GEN-LAST:event_button1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JanelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JanelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JanelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JanelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new JanelaInicial().setVisible(true);
            }
        });
    }
    
    public boolean getIsPlaying (){
        return isPlaying;
    }
    
    public void setIsPlaying (boolean isPlaying){
        this.isPlaying = isPlaying;
    }
    
    public int getLargura (){
        return largura;
    }
    
    public int getAltura (){
        return altura;
    }
    
    public int[][][] getMapa(){
        return mapa;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Button Jogar;
    private java.awt.Button button1;
    private java.awt.Label label1;
    private java.awt.Label label2;
    // End of variables declaration//GEN-END:variables
}