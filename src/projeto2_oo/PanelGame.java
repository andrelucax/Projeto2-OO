/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto2_oo;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;

/**
 *
 * @author andre
 */
public class PanelGame extends JPanel{
    private javax.swing.JToggleButton ataque2x2Button;
    private javax.swing.JToggleButton ataqueColunaButton;
    private javax.swing.JToggleButton ataqueLinhaButton;
    private javax.swing.JToggleButton ataqueUnicoButton;
    private javax.swing.JToggleButton descoberta2x2Button;
    private JLabel recursos;
    private final JLabel precoAtaque2x2;
    private final JLabel precoAtaqueColuna;
    private final JLabel precoAtaqueLinha;
    private final JLabel precoDescoberta2x2;
    private final JLabel precoAtaqueUnico;
    private final JButton infoButton;
    final int NENHUMATAQUE = 0;
    final int ATAQUECOLUNA = 5;
    final int ATAQUELINHA = 4;
    final int ATAQUE2X2 = 3;
    final int DESCOBERTA2X2 = 2;
    final int ATAQUEUNICO = 1;
    private int atingidos;
    private int total;
            
    private int ataqueEscolhido = NENHUMATAQUE;
    
    public PanelGame(int largura, int altura, Jogador jogador) {
        atingidos = 0;
        total = 0;
        for(int aux = 0; aux < largura; aux++){
            for(int aux2 = 0; aux2 < altura;aux2++){
                int aux3 = jogador.getMapa()[aux][aux2][Jogador.EMBARCACAOMAPA];
                if (aux3 != 0){
                    total++;
                }
                //System.out.print(Integer.toString(aux3));
            }
            //System.out.println("");
        }
        //System.out.println(Integer.toString(total));

        precoAtaque2x2 = new JLabel("Preco: 50 por celula aberta");
        precoAtaqueColuna = new JLabel("Preco: 50 por celula aberta");
        precoAtaqueLinha = new JLabel("Preco: 50 por celula aberta");
        precoDescoberta2x2 = new JLabel("Preco: 30 por celula aberta");
        precoAtaqueUnico = new JLabel("Preco: 50");
        infoButton = new JButton("Info");
        infoButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JOptionPane.showMessageDialog(null, "TUTORIAL:\n\nCelula azul = agua atingida, celula vermelha = embarcacao atingida, celula verde = embarcacao destruida\n"+
                                                    "Celula cinza = agua descoberta, celula rosa = embarcacao descoberta\n"+
                                                    "Na descoberta e ataque 2x2 é considerado o quadrado formado pelo ponto clicado, seu adjacente direito e os 2 abaixo dos citatos\n\n"+
                                                    "Informacoes no butão info");
            }
        });
        
        jogador.setRecursos((50*total) + (altura*largura - total)*25);
        ataqueUnicoButton = new JToggleButton("Atacar unica celula");
        ataqueUnicoButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (!ataqueUnicoButton.isSelected()){
                ataqueEscolhido = NENHUMATAQUE;
                }

                else {
                    ataqueEscolhido = ATAQUEUNICO;
                }

                if (ataque2x2Button.isSelected()){
                    ataque2x2Button.setSelected(false);
                }
                if (ataqueColunaButton.isSelected()){
                    ataqueColunaButton.setSelected(false);
                }
                if (ataqueLinhaButton.isSelected()){
                    ataqueLinhaButton.setSelected(false);
                }
                if (descoberta2x2Button.isSelected()){
                    descoberta2x2Button.setSelected(false);
                }
            }
        });
        descoberta2x2Button = new JToggleButton("Descobrir area 2x2");
        descoberta2x2Button.setPreferredSize(ataqueUnicoButton.getPreferredSize());
        descoberta2x2Button.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (!descoberta2x2Button.isSelected()){
                    ataqueEscolhido = NENHUMATAQUE;
                }

                else {
                    ataqueEscolhido = DESCOBERTA2X2;
                }

                if (ataque2x2Button.isSelected()){
                    ataque2x2Button.setSelected(false);
                }
                if (ataqueColunaButton.isSelected()){
                    ataqueColunaButton.setSelected(false);
                }
                if (ataqueLinhaButton.isSelected()){
                    ataqueLinhaButton.setSelected(false);
                }
                if (ataqueUnicoButton.isSelected()){
                    ataqueUnicoButton.setSelected(false);
                }
            }
        });
        ataque2x2Button = new JToggleButton("Atacar area 2x2");
        ataque2x2Button.setPreferredSize(ataqueUnicoButton.getPreferredSize());
        ataque2x2Button.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (!ataque2x2Button.isSelected()){
                    ataqueEscolhido = NENHUMATAQUE;
                }

                else {
                    ataqueEscolhido = ATAQUE2X2;
                }

                if (ataqueUnicoButton.isSelected()){
                    ataqueUnicoButton.setSelected(false);
                }
                if (ataqueColunaButton.isSelected()){
                    ataqueColunaButton.setSelected(false);
                }
                if (ataqueLinhaButton.isSelected()){
                    ataqueLinhaButton.setSelected(false);
                }
                if (descoberta2x2Button.isSelected()){
                    descoberta2x2Button.setSelected(false);
                }
            }
        });
        ataqueLinhaButton = new JToggleButton("Atacar linha");
        ataqueLinhaButton.setPreferredSize(ataqueUnicoButton.getPreferredSize());
        ataqueLinhaButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (!ataqueLinhaButton.isSelected()){
                    ataqueEscolhido = NENHUMATAQUE;
                }

                else {
                    ataqueEscolhido = ATAQUELINHA;
                }

                if (ataque2x2Button.isSelected()){
                    ataque2x2Button.setSelected(false);
                }
                if (ataqueColunaButton.isSelected()){
                    ataqueColunaButton.setSelected(false);
                }
                if (ataqueUnicoButton.isSelected()){
                    ataqueUnicoButton.setSelected(false);
                }
                if (descoberta2x2Button.isSelected()){
                    descoberta2x2Button.setSelected(false);
                }
            }
        });
        ataqueColunaButton = new JToggleButton("Atacar coluna");
        ataqueColunaButton.setPreferredSize(ataqueUnicoButton.getPreferredSize());
        ataqueColunaButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (!ataqueColunaButton.isSelected()){
                    ataqueEscolhido = NENHUMATAQUE;
                }

                else {
                    ataqueEscolhido = ATAQUECOLUNA;
                }

                if (ataque2x2Button.isSelected()){
                    ataque2x2Button.setSelected(false);
                }
                if (descoberta2x2Button.isSelected()){
                    descoberta2x2Button.setSelected(false);
                }
                if (ataqueLinhaButton.isSelected()){
                    ataqueLinhaButton.setSelected(false);
                }
                if (ataqueUnicoButton.isSelected()){
                    ataqueUnicoButton.setSelected(false);
                }
            }
        });
        
        recursos = new JLabel("Total recursos: "+Integer.toString(jogador.getRecursos()));
        
        
        
        
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        
        
        for (int aux = 0; aux < 6; aux++){
            gbc.gridx = largura+1;
            gbc.gridy = aux;
            if (aux == 0){
                add(recursos,gbc);
            }
            if (aux == 1){
                add(ataqueUnicoButton,gbc);
                gbc.gridx = largura+2;
                add(precoAtaqueUnico,gbc);
            }
            if (aux == 2){
                add(descoberta2x2Button,gbc);
                gbc.gridx = largura+2;
                add(precoDescoberta2x2,gbc);
            }
            if (aux == 3){
                add(ataque2x2Button,gbc);
                gbc.gridx = largura+2;
                add(precoAtaque2x2,gbc);
            }
            if (aux == 4){
                add(ataqueLinhaButton,gbc);
                gbc.gridx = largura+2;
                add(precoAtaqueLinha,gbc);
            }
            if (aux == 5){
                add(ataqueColunaButton,gbc);
                gbc.gridx = largura+2;
                add(precoAtaqueColuna,gbc);
            }
        }
        
        if (altura == 6){
            gbc.gridx = largura+1;
            gbc.gridy = altura;
            add(infoButton,gbc);
        }
        
        else{
            gbc.gridx = largura+1;
            gbc.gridy = altura-1;
            add(infoButton,gbc);
        }
        
        
        
        
        for (int row = 0; row < altura; row++) {
            for (int col = 0; col < largura; col++) {
                gbc.gridx = col;
                gbc.gridy = row;
                
                CelulaJogo celulaJogo = new CelulaJogo(largura, altura, jogador);
                celulaJogo.setPosicao(col, row);
                Border border = null;
                if (row < altura - 1) {
                    if (col < largura - 1) {
                        border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
                    } 
                    else {
                        border = new MatteBorder(1, 1, 0, 1, Color.GRAY);
                    }
                } 
                else {
                    if (col < largura - 1) {
                        border = new MatteBorder(1, 1, 1, 0, Color.GRAY);
                    } 
                    else {
                        border = new MatteBorder(1, 1, 1, 1, Color.GRAY);
                    }
                }
                celulaJogo.setBorder(border);
                add(celulaJogo, gbc);
            }
        }
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(jogador.getIsPlaying()){
                    if(ataqueEscolhido == ATAQUEUNICO){
                        try{
                            Component celulaClickada = getComponentAt((int)e.getPoint().getX(), (int) e.getPoint().getY());

                            if ("projeto2_oo.CelulaJogo".equals(celulaClickada.getClass().getName())){
                                CelulaJogo celula = (CelulaJogo) celulaClickada;
                                if (jogador.getRecursos() >= 50 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] != 0 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] != 1){
                                    tiroAcertado(jogador,celula);          
                                }
                                else if (jogador.getRecursos() >= 50 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] != 1){
                                    tiroNaAgua(jogador, celula);
                                }
                            }                                               
                        }
                        catch(Exception exp){
                        }  
                        verificaEmbarcacaoDestruidas(jogador, (int)e.getPoint().getX(), (int)e.getPoint().getY());
                    }
                    else if (ataqueEscolhido == ATAQUE2X2){
                        for (int aux = 0; aux < 2; aux++){
                            if (jogador.getRecursos() < 50){
                                break;
                            }
                            Component celulaClickada = getComponentAt((int)e.getPoint().getX() + aux*40, (int) e.getPoint().getY());
                            if(!"projeto2_oo.CelulaJogo".equals(celulaClickada.getClass().getName())){
                                    break;
                                }
                            for(int aux2 = 0; aux2 < 2; aux2++){
                                try{
                                    if (jogador.getRecursos() < 50){
                                        break;
                                    }
                                    celulaClickada = getComponentAt((int)e.getPoint().getX() + aux*40, (int) e.getPoint().getY()+aux2*40);
                                    if ("projeto2_oo.CelulaJogo".equals(celulaClickada.getClass().getName())){
                                        CelulaJogo celula = (CelulaJogo) celulaClickada;
                                        if (jogador.getRecursos() >= 50 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] != 0 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] != 1){
                                            tiroAcertado(jogador,celula);
                                        }
                                        else if (jogador.getRecursos() >= 50 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] != 1){
                                            tiroNaAgua(jogador, celula);
                                        }
                                    }
                                }
                                catch(Exception exp){
                                }
                            }
                        }
                    }

                    else if (ataqueEscolhido == DESCOBERTA2X2){
                        for (int aux = 0; aux < 2; aux++){
                            Component celulaClickada = getComponentAt((int)e.getPoint().getX() + aux*40, (int) e.getPoint().getY());
                            if(!"projeto2_oo.CelulaJogo".equals(celulaClickada.getClass().getName())){
                                break;
                            }
                            for(int aux2 = 0; aux2 < 2; aux2++){
                                try{
                                    if (jogador.getRecursos() < 50){
                                        break;
                                    }
                                    celulaClickada = getComponentAt((int)e.getPoint().getX() + aux*40, (int) e.getPoint().getY()+aux2*40);

                                    if ("projeto2_oo.CelulaJogo".equals(celulaClickada.getClass().getName())){
                                        CelulaJogo celula = (CelulaJogo) celulaClickada;
                                        if (jogador.getRecursos() >= 40 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] != 0 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] == 0){
                                            celula.setBackground(Color.PINK);
                                            int[][][] mapaAux = jogador.getMapa();
                                            mapaAux[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] = 2;
                                            jogador.setMapa(mapaAux);
                                            jogador.setRecursos(jogador.getRecursos() - 40);
                                            recursos.setText("Total recursos: "+ Integer.toString(jogador.getRecursos()));
                                        }
                                        else if (jogador.getRecursos() >= 40 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] == 0){
                                            celula.setBackground(Color.LIGHT_GRAY);
                                            int[][][] mapaAux = jogador.getMapa();
                                            mapaAux[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] = 2;
                                            jogador.setMapa(mapaAux);
                                            jogador.setRecursos(jogador.getRecursos() - 40);
                                            recursos.setText("Total recursos: "+ Integer.toString(jogador.getRecursos()));
                                        }
                                    }
                                }
                                catch(Exception exp){
                                }
                            }
                            if (jogador.getRecursos() < 50){
                                break;
                            }
                        }
                    }
                    else if (ataqueEscolhido == ATAQUELINHA){
                        for (int aux = 0; aux < largura; aux++){
                            try{
                                if (jogador.getRecursos() < 50){
                                    break;
                                }
                                Component celulaClickada = getComponentAt((int)e.getPoint().getX() + aux*40, (int) e.getPoint().getY());
                                if(!"projeto2_oo.CelulaJogo".equals(celulaClickada.getClass().getName())){
                                    break;
                                }
                                if ("projeto2_oo.CelulaJogo".equals(celulaClickada.getClass().getName())){
                                    CelulaJogo celula = (CelulaJogo) celulaClickada;
                                    if (jogador.getRecursos() >= 50 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] != 0 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] != 1){
                                        tiroAcertado(jogador,celula);
                                    }
                                    else if (jogador.getRecursos() >= 50 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] != 1){
                                        tiroNaAgua(jogador, celula);
                                    }
                                }
                            }
                            catch(Exception exp){
                            }
                        }
                        for (int aux = 0; aux < largura; aux++){
                            try{
                                if (jogador.getRecursos() < 50){
                                    break;
                                }
                                if (jogador.getRecursos() < 50){
                                    break;
                                }
                                Component celulaClickada = getComponentAt((int)e.getPoint().getX() - aux*40, (int) e.getPoint().getY());
                                if(!"projeto2_oo.CelulaJogo".equals(celulaClickada.getClass().getName())){
                                    break;
                                }
                                if ("projeto2_oo.CelulaJogo".equals(celulaClickada.getClass().getName())){
                                    CelulaJogo celula = (CelulaJogo) celulaClickada;
                                    if (jogador.getRecursos() >= 50 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] != 0 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] != 1){
                                        tiroAcertado(jogador,celula);
                                    }
                                    else if (jogador.getRecursos() >= 50 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] != 1){
                                        tiroNaAgua(jogador, celula);
                                    }

                                }
                            }
                            catch(Exception exp){
                            }    
                        }
                    }

                    else if (ataqueEscolhido == ATAQUECOLUNA){
                        for (int aux = 0; aux < altura; aux++){
                            try{
                                if (jogador.getRecursos() < 50){
                                    break;
                                }
                                Component celulaClickada = getComponentAt((int)e.getPoint().getX(), (int) e.getPoint().getY()+aux*40);
                                if(!"projeto2_oo.CelulaJogo".equals(celulaClickada.getClass().getName())){
                                    break;
                                }
                                if ("projeto2_oo.CelulaJogo".equals(celulaClickada.getClass().getName())){
                                    CelulaJogo celula = (CelulaJogo) celulaClickada;
                                    if (jogador.getRecursos() >= 50 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] != 0 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] != 1){
                                        tiroAcertado(jogador,celula);
                                    }
                                    else if (jogador.getRecursos() >= 50 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] != 1){
                                        tiroNaAgua(jogador, celula);
                                    }

                                }
                            }
                            catch(Exception exp){
                            }    
                        }
                        for (int aux = 0; aux < altura; aux++){
                            if (jogador.getRecursos() < 50){
                                break;
                            }
                            try{
                                if (jogador.getRecursos() < 50){
                                    break;
                                }
                                Component celulaClickada = getComponentAt((int)e.getPoint().getX(), (int) e.getPoint().getY() - aux*40);
                                if(!"projeto2_oo.CelulaJogo".equals(celulaClickada.getClass().getName())){
                                    break;
                                }
                                if ("projeto2_oo.CelulaJogo".equals(celulaClickada.getClass().getName())){
                                    CelulaJogo celula = (CelulaJogo) celulaClickada;
                                    if (jogador.getRecursos() >= 50 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] != 0 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] != 1){
                                        tiroAcertado(jogador,celula);
                                    }
                                    else if (jogador.getRecursos() >= 50 && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] != 1){
                                        tiroNaAgua(jogador, celula);
                                    }

                                }

                            }
                            catch(Exception exp){
                            }    
                        }
                    }
                    if (ataqueEscolhido != ATAQUEUNICO){
                        verificaEmbarcacaoCompleta(jogador, (int)e.getPoint().getX(), (int)e.getPoint().getY());
                    }
                    if (total == atingidos){
                        escreverRecord(jogador.getRecursos());
                        JOptionPane.showMessageDialog(null, "Você venceu");
                        jogador.setIsPlaying(false);
                        //System.exit(0);
                    }
                    else if (jogador.getRecursos() < 50){
                        JOptionPane.showMessageDialog(null, "Você perdeu");
                        jogador.setIsPlaying(false);
                        //System.exit(0);
                    }
                }
            }
        });
    }
    
    private void verificaEmbarcacaoDestruidas(Jogador jogador, int x, int y){
        Component celulaClickada = getComponentAt(x, y);
        int horizontal = 1;
        int vertical = 1;
        try{
            CelulaJogo celula = (CelulaJogo) celulaClickada;
            int tamanhoEmbarcacao = jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA];
            for(int aux = 1; aux < tamanhoEmbarcacao; aux++){
                // Pra cima
                celulaClickada = getComponentAt(x + aux*40, y);
                try{
                    celula = (CelulaJogo) celulaClickada;
                    if (jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] == tamanhoEmbarcacao && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] == 1){
                        horizontal++;
                    }
                    else{
                        break;
                    }
                }
                catch(Exception exp){
                    break;
                }

            }
            for(int aux = 1; aux < tamanhoEmbarcacao; aux++){
                // Pra baixo
                celulaClickada = getComponentAt(x - aux*40, y);
                try{
                    celula = (CelulaJogo) celulaClickada;
                    if (jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] == tamanhoEmbarcacao && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] == 1){
                        horizontal++;
                    }
                    else{
                        break;
                    }
                }
                catch(Exception exp){
                    break;
                }
            }
            for(int aux = 1; aux < tamanhoEmbarcacao; aux++){
                // Pra esquerda
                celulaClickada = getComponentAt(x, y - aux*40);
                try{
                    celula = (CelulaJogo) celulaClickada;
                    if (jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] == tamanhoEmbarcacao && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] == 1){
                        vertical++;
                    }
                    else{
                        break;
                    }
                }
                catch(Exception exp){
                    break;
                }
            }
            for(int aux = 1; aux < tamanhoEmbarcacao; aux++){
                // Pra direita
                celulaClickada = getComponentAt(x, y + aux*40);
                try{
                    celula = (CelulaJogo) celulaClickada;
                    if (jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] == tamanhoEmbarcacao && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] == 1){
                        vertical++;
                    }
                    else{
                        break;
                    }
                }
                catch(Exception exp){
                    break;
                }
            }
            //System.out.println(Integer.toString(horizontal));  
            if(horizontal == tamanhoEmbarcacao){
                for(int aux = 0; aux < tamanhoEmbarcacao; aux++){
                    // Pra cima
                    celulaClickada = getComponentAt(x + aux*40, y);
                    try{
                        celula = (CelulaJogo) celulaClickada;
                        if (jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] == tamanhoEmbarcacao && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] == 1){
                            celula.setBackground(Color.GREEN);
                        }
                        else{
                            break;
                        }
                    }
                    catch(Exception exp){
                        break;
                    }
                }

                for(int aux = 0; aux < tamanhoEmbarcacao; aux++){
                    // Pra cima
                    celulaClickada = getComponentAt(x - aux*40, y);
                    try{
                        celula = (CelulaJogo) celulaClickada;
                        if (jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] == tamanhoEmbarcacao && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] == 1){
                            celula.setBackground(Color.GREEN);
                        }
                        else{
                            break;
                        }
                    }
                    catch(Exception exp){
                        break;
                    }
                }
            }
            else if (vertical == tamanhoEmbarcacao){
                for(int aux = 0; aux < tamanhoEmbarcacao; aux++){
                    // Pra cima
                    celulaClickada = getComponentAt(x, y +aux*40);
                    try{
                        celula = (CelulaJogo) celulaClickada;
                        if (jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] == tamanhoEmbarcacao && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] == 1){
                            celula.setBackground(Color.GREEN);
                        }
                        else{
                            break;
                        }
                    }
                    catch(Exception exp){
                        break;
                    }
                }

                for(int aux = 0; aux < tamanhoEmbarcacao; aux++){
                    // Pra cima
                    celulaClickada = getComponentAt(x, y- aux*40);
                    try{
                        celula = (CelulaJogo) celulaClickada;
                        if (jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.EMBARCACAOMAPA] == tamanhoEmbarcacao && jogador.getMapa()[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] == 1){
                            celula.setBackground(Color.GREEN);
                        }
                        else{
                            break;
                        }
                    }
                    catch(Exception exp){
                        break;
                    }
                }
            }
        }
        catch(Exception exp){
            
        }
    }
    
    private void verificaEmbarcacaoCompleta(Jogador jogador, int xIni, int yIni){
        int x = xIni;
        int y = yIni;
        
        for(int aux1 = 0; aux1 < 15; aux1++){
            for(int aux2 = 0; aux2 < 15; aux2++){
                x = x-aux1*40;
                y = y-aux2*40;
                verificaEmbarcacaoDestruidas(jogador, x, y);
                x = xIni;
                y = yIni;
            }
        }
        for(int aux1 = 0; aux1 < 15; aux1++){
            for(int aux2 = 0; aux2 < 15; aux2++){
                x = x-aux1*40;
                y = y+aux2*40;
                verificaEmbarcacaoDestruidas(jogador, x, y);
                x = xIni;
                y = yIni;
            }
        }
        for(int aux1 = 0; aux1 < 15; aux1++){
            for(int aux2 = 0; aux2 < 15; aux2++){
                x = x+aux1*40;
                y = y+aux2*40;
                verificaEmbarcacaoDestruidas(jogador, x, y);
                x = xIni;
                y = yIni;
            }
        }
        for(int aux1 = 0; aux1 < 15; aux1++){
            for(int aux2 = 0; aux2 < 15; aux2++){
                x = x+aux1*40;
                y = y-aux2*40;
                verificaEmbarcacaoDestruidas(jogador, x, y);
                x = xIni;
                y = yIni;
            }
        }
    }
    
    private void tiroAcertado(Jogador jogador, CelulaJogo celula){
        celula.setBackground(Color.RED);
        atingidos++;
        int[][][] mapaAux = jogador.getMapa();
        mapaAux[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] = 1;
        jogador.setMapa(mapaAux);
        celula.setClicked(true);
        jogador.setRecursos(jogador.getRecursos() - 50);
        recursos.setText("Total recursos: "+ Integer.toString(jogador.getRecursos()));
    }
    
    private void tiroNaAgua(Jogador jogador, CelulaJogo celula){
        int[][][] mapaAux = jogador.getMapa();
        mapaAux[celula.getPosicao()[0]][celula.getPosicao()[1]][Jogador.FIREDMAPA] = 1;
        jogador.setMapa(mapaAux);
        celula.setBackground(Color.BLUE);
        
        celula.setLayout(null);
        ImageIcon imagemIcon = new ImageIcon("images/agua.gif");
        Image image = imagemIcon.getImage();
        JLabel aguaGif = new JLabel(new ImageIcon(image));        
        celula.add(aguaGif);
        aguaGif.setBounds(0, 0, 40, 40);
        celula.repaint();
        
        celula.setClicked(true);
        jogador.setRecursos(jogador.getRecursos() - 50);
        recursos.setText("Total recursos: "+ Integer.toString(jogador.getRecursos()));
    }
    
    private void escreverRecord(int recordAtual){
        Records recordsAntigos = new Records();
        FileWriter arq = null;
        PrintWriter printToFile = null;
        for(int aux = 0; aux < 10; aux++){
            if(recordsAntigos.getRecord()[aux] < recordAtual){
                int[] recordsNovos = recordsAntigos.getRecord();
                recordsNovos[aux] = recordAtual;
                try{
                    String record = "";
                    for (int aux2 = 0; aux2 < 10; aux2++){
                        record = record + String.valueOf(recordsNovos[aux2] + "\n");
                    }
                    //System.out.println(record);
                    arq = new FileWriter("record/records.txt");
                    arq.write(record);
                    arq.close();
                    //printToFile = new PrintWriter(arq);
                    //printToFile.print(record);
                }
                catch(Exception e){}
                
                break;
            }
        }
    }
}