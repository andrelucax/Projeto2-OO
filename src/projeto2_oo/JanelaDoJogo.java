/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto2_oo;

import javax.swing.JFrame;

/**
 *
 * @author andre
 */
public class JanelaDoJogo extends JFrame {
    JFrame janelaJogo = new JFrame();
    public JanelaDoJogo(int largura, int altura, Jogador jogador){
        janelaJogo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        //janelaJogo.setResizable(false);
        janelaJogo.add(new PanelGame(largura, altura, jogador));
        janelaJogo.pack();
        janelaJogo.setVisible(true);
        //janelaJogo.setExtendedState(MAXIMIZED_BOTH);
    }
    
    @Override
    public void setVisible(boolean visible){
        janelaJogo.setVisible(visible);
    }
    
    @Override
    public void setExtendedState(int state){
        janelaJogo.setExtendedState(state);
    }
}