/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto2_oo;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;

/**
 *
 * @author andre
 */
public class CelulaJogo extends JPanel {

    private Color defaultBackground;
    private int[] posicao = new int[2];
    private boolean clicked = false;
    
    public CelulaJogo(int largura, int altura, Jogador jogador) {
        
        /*addMouseListener(new MouseAdapter() {
            //@Override
            //public void mouseEntered(MouseEvent e) {
               // if (!clicked){
                //    defaultBackground = getBackground();
                 //   setBackground(Color.GRAY);
               // }
           // }

            @Override
            public void mouseExited(MouseEvent e) {
                if (!clicked){
                    setBackground(defaultBackground);
                }
            }
        });*/
    }
    
    public void setPosicao(int linha, int coluna){
        posicao[0] = linha;
        posicao[1] = coluna;
    }
    
    public CelulaJogo getCelulaJogo (){
        return this;
    }
    
    public int[] getPosicao(){
        return posicao;
    }
    
    public void setClicked(boolean cliked){
        this.clicked = clicked;
    }
    
    public boolean getClicked(){
        return clicked;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(40, 40);
    }
}