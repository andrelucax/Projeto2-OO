/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto2_oo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 *
 * @author andre
 */
public class Records {
    int[] record = new int[10];
    
    Records(){
        File arquivo = new File("record/records.txt");
        try{
            BufferedReader br = new BufferedReader(new FileReader(arquivo.getAbsolutePath()));
            for(int aux = 0; aux < 10; aux++){
                String auxS = br.readLine();
                record[aux] = Integer.parseInt(auxS);
            }
        }
        catch(Exception e){
            
        }
    }
    
    public void setRecord(int[] record){
        this.record = record;
    }
    
    public int[] getRecord(){
        return record;
    }
}
