/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto2_oo;

import static java.lang.Thread.sleep;

/**
 *
 * @author andre
 */
public class Projeto2_OO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here 
        JanelaInicial janelaInicial = new JanelaInicial();
        janelaInicial.setLocationRelativeTo(null);
        janelaInicial.setResizable(false);
        janelaInicial.setVisible(true);
        
        while (true){
            if (janelaInicial.getIsPlaying()){
                janelaInicial.setIsPlaying(false);
                Jogador jogador = new Jogador(janelaInicial.getLargura(), janelaInicial.getAltura());
                jogador.setMapa(janelaInicial.getMapa());
                janelaInicial.setVisible(false);
                JanelaDoJogo janelaJogo = new JanelaDoJogo(janelaInicial.getLargura(), janelaInicial.getAltura(), jogador);
                ThreadJanelaJogo thread = new ThreadJanelaJogo(janelaJogo);
                //thread.setRunning(true);
                thread.start();
                while(true){
                    if(!jogador.getIsPlaying()){
                        //thread.interrupt();
                        thread.setRunning(false);
                        janelaJogo.dispose();
                        janelaJogo.setVisible(false);
                        janelaInicial.setVisible(true);
                        break;
                    }
                    else{
                        try{
                            sleep(200);
                        }
                        catch(Exception e){}
                    }
                }
            }
            else{
                try{
                    sleep(200);
                }
                catch(Exception e){}
            }
        }
    }
}
