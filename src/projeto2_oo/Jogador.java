package projeto2_oo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author andre
 */
public class Jogador {    
    private int recursos;
    private int[][][] mapa;
    
    private boolean isPlaying;
    
    final static int NOTFIRED = 0;
    final static int FIRED = 1;
    
    final static int FIREDMAPA = 1;
    final static int EMBARCACAOMAPA = 0;
    
    Jogador(int largura, int altura){
        mapa = new int[largura][altura][2];
        isPlaying = true;
        recursos = 0;
        for(int aux = 0;aux<largura;aux++){
            for(int aux2=0;aux2<altura;aux2++){
                mapa[aux][aux2][0] = 0;
                mapa[aux][aux2][1] = 0;
            }
        }
    }
    
    public boolean getIsPlaying(){
        return isPlaying;
    }
    
    public void setIsPlaying(boolean isPlaying){
        this.isPlaying = isPlaying;
    }
    
    public int[][][] getMapa(){
        return mapa;
    }
    
    public void setMapa(int[][][] mapa){
        this.mapa = mapa;
    }
    
    public int getRecursos(){
        return recursos;
    }
    
    public void setRecursos(int recursos){
        this.recursos = recursos;
    }
}
