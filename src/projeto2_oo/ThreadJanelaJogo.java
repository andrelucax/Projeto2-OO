/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto2_oo;

/**
 *
 * @author andre
 */
public class ThreadJanelaJogo extends Thread{
    boolean running;
    JanelaDoJogo janelaJogo;

    public ThreadJanelaJogo(JanelaDoJogo janelaJogo) {
        this.running = true;
        this.janelaJogo = janelaJogo;
    }
    
    @Override
    public void run(){
        while(true){
            if(running == false){
                //System.out.println("projeto2_oo.ThreadJanelaJogo.run()");
                janelaJogo.setVisible(false);
                break;
            }
            try{
                janelaJogo.setExtendedState(JanelaDoJogo.MAXIMIZED_BOTH);
                sleep(500);
            }
            catch(Exception e){
                running = false;
            }
        }
    }
    
    public void setRunning (boolean running){
        this.running = running;
    }
}
